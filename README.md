# Lab02

This is lab 2 post lab questions:

1.Suppose the duty ratio of a LCD display is ¼ and it has a total of 120 display segments (pixels). How many pins are required to drive this LCD?
An LCD display of duty ratio 1/4 needs 4 pins for common plane (COM1, COM2, COM3, COM4) and 1 pin for every four display segments. 
number of pins = 4 + (120/4)
                = 4 + 30 = 34
34 pins are required to drive an LCD of duty ratio 1/4 having 120 display segments.

2. Is the LCD driver (programmed in this lab) built in within the processor chip? What is the function of the COM driver and SEG driver?
Yes, the STM32 uses the internal hardware driver which is built into the microproccesor chip. The drivers are in charge of enabling and disabling each segment by giving the same voltage wave form for the COM and the SEG line in order to pass the polarizors voltage threshhold. The divers are also in charge of alternativly turning on and off the voltages in order to make sure the polarizors dont turn off.

3. How many pixels can the STM32L4 processor LCD driver drive? How large is the LCD_RAM in terms of bits? (Look to STM32L4 reference manual for answers)
LCD driver with upto 8 common terminals and 44 segment terminals can drive upto 176 (i.e. 44*4) or 320 (i.e. 40*8) picture elements or pixels. LCD_RAM can be as large as upto 16*32 bit registers ( two 32-bit words).

4. How many pixels does the LCD installed on the STM32L4 discovery kit have?
128 * 160 PIXEL used in LCD instalation on the STM32L discovery kit. Th unused LCD RAM REGISTERS can be spared due to its future utilization.

5. If you only pass a 3-character long string to your LCD_Display_String() function, but your printing loop does not check for string length and tries to print characters 4, 5, and 6, what will get printed to the display there?
It will print out the first three characters correctly and for the next three characters it will display the values in the next three addresses next to the character array.